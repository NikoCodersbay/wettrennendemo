package race;

import vehicles.Fahrzeug;

import java.util.ArrayList;
import java.util.List;

public class Wettrennen {

    private double trackLength;

    private double refreshRate;

    private boolean raceOnGoing = false;

    private List<Fahrzeug> fahrzeugList = new ArrayList<>();

    public Wettrennen(double trackLength, double refreshRate) {
        this.trackLength = trackLength;
        this.refreshRate = refreshRate;
    }

    public void prepareRace(){
        Fahrzeug fahrzeug1 = new Fahrzeug(4.0, 350.0, "Porsche");
        Fahrzeug fahrzeug2 = new Fahrzeug(2.0, 150.0, "Yugo");
        Fahrzeug fahrzeug3 = new Fahrzeug(5.0, 250, "BMW");

        fahrzeugList.add(fahrzeug1);
        fahrzeugList.add(fahrzeug2);
        fahrzeugList.add(fahrzeug3);

        startRace();
    }

    public void startRace(){
        raceOnGoing = true;

        while (raceOnGoing){
            moveVehicles();
        }

        printWinner();
    }

    public void moveVehicles(){
        for (Fahrzeug fahrzeug: fahrzeugList){
            fahrzeug.move(refreshRate);
            checkWinConditions(fahrzeug);
        }
    }

    public void checkWinConditions(Fahrzeug fahrzeug){

        if (fahrzeug.getPosition() >= trackLength){
            raceOnGoing = false;
        }
    }

    public void printWinner(){
        Fahrzeug winningFahrezug = null;
        double highestPosition = 0.0;
        for (Fahrzeug fahrzeug: fahrzeugList){
            if (fahrzeug.getPosition() >= highestPosition){
                highestPosition = fahrzeug.getPosition();
                winningFahrezug = fahrzeug;
            }
        }
        System.out.println("Gewinner ist " + winningFahrezug.getName());
    }

    public double getTrackLength() {
        return trackLength;
    }

    public void setTrackLength(double trackLength) {
        this.trackLength = trackLength;
    }

}
