package vehicles;

public class Fahrzeug {

    private String name;

    private double position = 0;

    private double currentSpeed = 0;

    //in m/s
    private double accelerationRate;

    private double maxSpeed;

    public Fahrzeug(double accelerationRate, double maxSpeed, String name) {
        this.accelerationRate = accelerationRate;
        this.maxSpeed = maxSpeed;
        this.name = name;
    }

    public void move(double sec){
        currentSpeed = Math.min(maxSpeed, accelerationRate * sec + currentSpeed);
        position += currentSpeed * sec;
    }

    public double getPosition() {
        return position;
    }

    public void setPosition(double position) {
        this.position = position;
    }

    public double getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(double currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public double getAccelerationRate() {
        return accelerationRate;
    }

    public void setAccelerationRate(double accelerationRate) {
        this.accelerationRate = accelerationRate;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
