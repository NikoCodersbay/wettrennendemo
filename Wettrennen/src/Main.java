import race.Wettrennen;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wähle die Strecken länge");
        double length = Double.parseDouble(scanner.nextLine());

        Wettrennen wettrennen = new Wettrennen(length, 1);

        wettrennen.prepareRace();
    }
}